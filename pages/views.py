# pages/views.py
from django.http import HttpResponse


# Create your views here.
def HomePageView(request):
    return HttpResponse('HomePage')

def AboutPageView(request):
    return HttpResponse('About page')

def ContactPageView(request):
    return  HttpResponse('Contact page')

